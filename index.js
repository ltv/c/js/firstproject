const express = require('express');
const app = express();
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const port = 3000;

// users tmp
const users = [
    {id: 1, name: "Nguyen Huu Tien", email: "nguyenhuutien.it.3895@gmail.com", age: 24}, 
    {id: 2, name: "Hoang Hiep", email: "@hoanghiep4298", age: 18},
    {id: 3, name: "Duong Quoc Cuong", email: "@duongquoccuongkb81", age: 18}
];

app.get('/', (req,res) => {
    res.send( 'Hello World' );
});

app.get('/users', (req,res) => {
    res.render('users/index', { users: users });
});

app.get('/users/search', (req,res) => {
    // search and return here

    var queryName = req.query.name;
    var queryAge  = req.query.age;

    var result = users.filter( user => {
        return user.name.toLowerCase().indexOf(queryName.toLowerCase()) !== -1 && user.age === parseInt(queryAge);
    });

    res.render('users/index', {
        users: result
    });
});

app.get('/users/create', (req,res) => {
    res.render('users/create');
});

app.post('/users/create', (req,res) => {
    // console.log(req.body)
    users.push(req.body);
    res.redirect('/users');
});

app.get('/users/:id', (req, res) => {
    // console.log(req.params);
    var user = users.find( (user) => {
		return user.id == parseInt(req.params.id);
    });
    
    res.render('users/show', {
    	user: user
    })
})

app.listen(port, () => {
    console.log( 'Your app running on port' + port );
});

